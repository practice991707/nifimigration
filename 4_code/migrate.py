import os
import json
import sys
import time
import locale
import logging
import nipyapi
import urllib3

def load_config():
    if len(sys.argv) < 2:
        print("Input File Missing")
        sys.exit()
    with open(sys.argv[1]) as file:
        return json.load(file)

def get_registry(nifi_api_url,nifi_username,nifi_password):
    if not nifi_api_url:
        print("Endpoints cannot be empty")
        return None     
    nipyapi.utils.set_endpoint(nifi_api_url)
    nipyapi.security.set_service_ssl_context(service='nifi', check_hostname=False)
    try:
        nipyapi.security.service_login(service="nifi", username=nifi_username, password=nifi_password)
    except:
        pass
    existing_registries = nipyapi.versioning.list_registry_clients()
    if not existing_registries:
        print("No registry found by api")
        return None
    for registry in existing_registries.registries:
        #if registry.component.uri+"/nifi-registry-api" == reg_api_url:
        return registry
    return None

def get_buckets(bucket_name,registry,registry_username,registry_password):
    nipyapi.utils.set_endpoint(registry+"/nifi-registry-api")
    nipyapi.security.set_service_ssl_context(service='registry', check_hostname=False)
    try:
        nipyapi.security.service_login(service="registry", username=registry_username, password=registry_password)
    except:
        pass
    existing_buckets = nipyapi.versioning.list_registry_buckets()
    for bucket in existing_buckets:
        if bucket.name == bucket_name:
            return bucket

def create_bucket(bucket_name,registry,registry_username,registry_password):
    nipyapi.utils.set_endpoint(registry+"/nifi-registry-api")
    nipyapi.security.set_service_ssl_context(service='registry', check_hostname=False)
    try:
        nipyapi.security.service_login(service="registry", username=registry_username, password=registry_password)
    except:
        pass
    try:
        return nipyapi.versioning.create_registry_bucket(bucket_name)
    except Exception as e:
        raise RuntimeError(f"Error creating bucket '{bucket_name}' in registry '{registry}': {e}")

def export_flow(registry, bucket, flow,registry_username,registry_password):
    nipyapi.utils.set_endpoint(registry+"/nifi-registry-api") 
    nipyapi.security.set_service_ssl_context(service='registry', check_hostname=False)
    try:
        nipyapi.security.service_login(service="registry", username=registry_username, password=registry_password)
    except:
        pass
    flow_id = nipyapi.versioning.get_flow_in_bucket(bucket.identifier, identifier=flow['name'])
    if not flow_id:
        return None
    if "version" in flow.keys():
        if flow['version']>flow_id.version_count:
            return nipyapi.versioning.export_flow_version(bucket_id=bucket.identifier, flow_id=flow_id.identifier, mode='yaml')     
        return nipyapi.versioning.export_flow_version(bucket_id=bucket.identifier, flow_id=flow_id.identifier, version=str(flow['version']), mode='yaml')
    else:
        return nipyapi.versioning.export_flow_version(bucket_id=bucket.identifier, flow_id=flow_id.identifier, mode='yaml')

def import_flow(registry, bucket, flow_name, flow_data, registry_username, registry_password):
    nipyapi.utils.set_endpoint(registry+"/nifi-registry-api")
    nipyapi.security.set_service_ssl_context(service='registry', check_hostname=False)
    try:   
        nipyapi.security.service_login(service="registry", username=registry_username, password=registry_password)
    except:
        pass
    dest_flow = nipyapi.versioning.get_flow_in_bucket(bucket_id=bucket.identifier, identifier=flow_name)
    if dest_flow is None:
        imported_flow = nipyapi.versioning.import_flow_version(bucket_id=bucket.identifier, encoded_flow=flow_data, flow_name=flow_name)
    else:
      imported_flow = nipyapi.versioning.import_flow_version(bucket_id=bucket.identifier, encoded_flow=flow_data, flow_id=dest_flow.identifier)
    return imported_flow


def add_flow_to_canvas(bucket, flow_info):
    config = load_config()
    target_config = config['ENV'][1]
    target_registry = get_registry(target_config["nifi_api_url"]+"/nifi-api",os.getenv('TARGET_NIFI_USER'),os.getenv('TARGET_NIFI_PASSWORD'))
    nipyapi.utils.set_endpoint(target_config["reg_api_url"]+"/nifi-registry-api")
    nipyapi.security.set_service_ssl_context(service='registry', check_hostname=False)
    try: 
        nipyapi.security.service_login(service="registry", username=os.getenv('TARGET_REG_USER'), password=os.getenv('TARGET_REG_PASSWORD'))
    except:
        pass
    flow_id = nipyapi.versioning.get_flow_in_bucket(bucket.identifier, identifier=flow_info['name'])
    if not flow_id:
        print(f"Flow {flow_info['name']} missing from target registry")
        return None
    nipyapi.utils.set_endpoint(target_config["nifi_api_url"]+"/nifi-api")
    nipyapi.security.set_service_ssl_context(service='nifi', check_hostname=False) 
    try:
        nipyapi.security.service_login(service="nifi", username=os.getenv('TARGET_NIFI_USER'), password=os.getenv('TARGET_NIFI_PASSWORD'))
    except:
        pass
    isNew = None
    pgs = nipyapi.canvas.list_all_process_groups(pg_id=nipyapi.canvas.get_root_pg_id())
    total = len(pgs)
    i = 0
    for pg in pgs:
        i += 1
        ver_info = pg.component.version_control_information
        if ver_info and ver_info.flow_id == flow_id.identifier:
            # update Canvas in place
            isNew = 1
            nipyapi.canvas.schedule_components(pg_id=pg.id, scheduled=False)
            all_connections =  nipyapi.canvas.list_all_connections(pg_id=pg.id)
            queued_count = sum(locale.atoi(connection.status.aggregate_snapshot.queued_count) for connection in all_connections)
            #Wait for Queues are empty
            while (queued_count > 0):
                print(f"There are still %d queued events, waiting for all are processed {queued_count}")
                time.sleep(10)
                all_connections =  nipyapi.canvas.list_all_connections(pg_id=pg.id)
                queued_count = sum(locale.atoi(connection.status.aggregate_snapshot.queued_count) for connection in all_connections)
            print(f"Process Group '{pg.component.name}' has no queued event, start updating new version now! Flow name: '{flow_info['name']}'")
            nipyapi.canvas.schedule_process_group(pg.id, scheduled=False)
            nipyapi.versioning.update_flow_ver(process_group=pg)
            nipyapi.canvas.schedule_process_group(pg.id, scheduled=True)
            nipyapi.canvas.schedule_components(pg_id=pg.id, scheduled=True)
    if not isNew:
        #deploy anew into the Canvas
        pg = nipyapi.versioning.deploy_flow_version(parent_id=nipyapi.canvas.get_root_pg_id(), location=(0, 0), bucket_id=bucket.identifier, flow_id=flow_id.identifier, reg_client_id=target_registry.id)
        nipyapi.versioning.update_flow_ver(process_group=pg)

def migrate_buckets_and_flows(source_registry, target_registry, buckets):
    config = load_config()
    source_config = config['ENV'][0]
    target_config = config['ENV'][1]
    for bucket in buckets:
        source_bucket = get_buckets(bucket['name'], source_registry,os.getenv('SOURCE_REG_USER'),os.getenv('SOURCE_REG_PASSWORD'))
        if not source_bucket:
            print(f"Error: source bucket '{bucket['name']}' in {source_config['name']} stage was not found.")
            continue
        target_bucket = get_buckets(bucket['name'], target_registry,os.getenv('TARGET_REG_USER'),os.getenv('TARGET_REG_PASSWORD'))
        if not target_bucket:
            target_bucket = create_bucket(bucket['name'], target_registry,os.getenv('TARGET_REG_USER'),os.getenv('TARGET_REG_PASSWORD'))

        if not target_bucket:
            print("Some kind of mistakes in target bucket")        
            continue
    
        for flow_info in bucket['flows']:
            if "version" in flow_info.keys():
                if isinstance(flow_info['version'], int) or flow_info['version']<=0:
                    flow_version = flow_info['version']
                else:
                    print(f"Error: version '{flow_info['version']}' of '{flow_info['name']}' seems wrong")
                    continue 
            else:
                flow_version = "latest"
            flow_data = export_flow(source_registry, source_bucket, flow_info,os.getenv('SOURCE_REG_USER'),os.getenv('SOURCE_REG_PASSWORD'))
            if not flow_data:
                print(f"Error: source flow '{flow_info['name']}' from bucket '{source_bucket.name}' in {source_config['name']} stage was not found.")
                continue
            import_flow(target_registry,target_bucket, flow_info['name'], flow_data,os.getenv('TARGET_REG_USER'),os.getenv('TARGET_REG_PASSWORD'))
            print(f"Successfully copied flow '{flow_info['name']}':'{flow_version}' from bucket '{source_bucket.name}' {source_config['name']} stage to bucket '{target_bucket.name}' {target_config['name']} stage")
            add_flow_to_canvas(target_bucket, flow_info)
            print(f"Successfully added flow '{flow_info['name']}' from bucket {target_bucket.name} {target_config['name']} to NiFi canvas.")

def main():
    config = load_config()
    source_config = config['ENV'][0]
    target_config = config['ENV'][1]
    buckets_info = config["buckets"]
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    nipyapi.config.registry_config.verify_ssl = False
    nipyapi.config.nifi_config.verify_ssl = False
    nipyapi.config.registry_config.verify_ssl = False
    try:
        target_registry = get_registry(target_config["nifi_api_url"]+"/nifi-api",os.getenv('TARGET_NIFI_USER'),os.getenv('TARGET_NIFI_PASSWORD'))
        if ((not source_config["reg_api_url"]) or (not source_config["reg_api_url"]) or (not target_registry)):
            print(f"Cannot get target/source registry or target nifi")
            return None
        migrate_buckets_and_flows(source_config["reg_api_url"], target_config["reg_api_url"], buckets_info)
    except Exception as e:
        print(f"Error occurred during migration: {e}")

if __name__ == "__main__":
    main()
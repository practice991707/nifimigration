Миграция между средами dev-qa, qa-dev для Nifi версии 1.х

Исходный код проекта находятся в папках с необходимым префиксом

1) Через Nifi toolset через cli
https://www.clearpeaks.com/versioning-nifi-flows-and-automating-their-deployment-with-nifi-registry-and-nifi-toolkit/

2) Через Python скрипт через запросы к restAPI
https://github.com/chamilad/nifi_cicd_poc/blob/main/migrate.py

3) Через Nifi toolset через cli, есть готовое решение для jenkins
https://github.com/InsightByte/ApacheNifi/tree/main/Chapter-7/Jenkins/Registry-Scripts

4) В ходе многочисленных тестов функционал решений не был удовлетворительным. В итоге, в ходе тестов принято решение написать свою обвязку к restAPI NiFi и NiFi registry. Исходный код проекта, который был предоставлен закачику находится в папке 
